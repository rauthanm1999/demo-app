import './App.css';
import { Routes, Route, Navigate } from "react-router-dom";
import Home from './modules/home';
import PageNot from './modules/pageNot';



function App() {
  
  return (
    <>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/*" element={<PageNot />} />

 </Routes>
    </>
  );
}

export default App
