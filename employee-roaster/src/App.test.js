// App.test.js
import React from 'react';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux'; // Import the Provider
import configureStore from 'redux-mock-store'; // Import a mock Redux store
import App from './App';
import Home from './modules/home';
import PageNot from './modules/pageNot';

const mockStore = configureStore(); // Create a mock Redux store

test('renders Home component for the root path', () => {
  const store = mockStore({}); // Initialize the mock store with an initial state
  const { getByText } = render(
    <Provider store={store}> {/* Provide the Redux store */}
      <MemoryRouter initialEntries={['/']}>
        <App />
      </MemoryRouter>
    </Provider>
  );

  expect(getByText(/Home/i)).toBeInTheDocument();
});

test('renders PageNot component for unknown paths', () => {
  const store = mockStore({});
  const { getByText } = render(
    <Provider store={store}>
      <MemoryRouter initialEntries={['/unknown']}>
        <App />
      </MemoryRouter>
    </Provider>
  );

  expect(getByText(/Page Not Found/i)).toBeInTheDocument();
});
