import React from 'react'

export default function Header({ companyInfo }) {
    return (
        <>
            <nav className="navbar navbar-expand-lg bg-body-tertiary d-block">
                <div className="container-fluid">
                    <a className="navbar-brand" href="#">{companyInfo?.companyName}</a>
                </div>
                <div className="container-fluid">
                    <div className='d-flex w-100 justify-content-xxl-between'>
                        <a className="navbar-brand" href="#">{companyInfo?.companyMotto}</a>

                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <a className="nav-link" href="#">Since ({companyInfo?.companyEst})</a>
                            </li>

                        </ul>
                    </div>
                </div>
               
            </nav>
        </>
    )
}
