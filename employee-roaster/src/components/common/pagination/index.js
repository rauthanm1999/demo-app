import React from 'react';

function Pagination({ itemsPerPage, totalItems, currentPage, onPageChange, showText }) {
    const pageNumbers = [];
    const totalPages = Math.ceil(totalItems / itemsPerPage);

    for (let i = 1; i <= totalPages; i++) {
        pageNumbers.push(i);
    }

    return (
        <nav className='navbar'>
            {showText && (
                <p className="pagination-text">
                    Showing {((currentPage - 1) * itemsPerPage) + 1} to{' '}
                    {currentPage * itemsPerPage} of {totalItems} items
                </p>
            )}
            <ul className="pagination">
                <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                    <a
                        href="#"
                        className="page-link"
                        onClick={() => onPageChange(currentPage - 1)}
                    >
                        Previous
                    </a>
                </li>
                {pageNumbers.map((number) => (
                    <li key={number} className={`page-item ${currentPage === number ? 'active' : ''}`}>
                        <a
                            onClick={() => onPageChange(number)}
                            href="#"
                            className="page-link"
                        >
                            {number}
                        </a>
                    </li>
                ))}
                <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                    <a
                        href="#"
                        className="page-link"
                        onClick={() => onPageChange(currentPage + 1)}
                    >
                        Next
                    </a>
                </li>
            </ul>
        </nav>
    );
}

export default Pagination;
