import React, { useState } from 'react'
import Modal from '../modal'

export default function Table({data}) {
    const [selectedCard, setSelectedCard] = useState(null);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const handleCardClick = (cardData) => {
        setSelectedCard(cardData);
        setIsModalOpen(true);
    };
  
    
  return (
      <div>
          <table className="table table-striped table-hover border">
          <thead>
              <tr>
                  <th scope="col">Id</th>
                  <th scope="col">Name</th>
                  <th scope="col">Contact No.</th>
                  <th scope="col">Address</th>
              </tr>
          </thead>
          <tbody>
            {
                  data?.map((i)=>{
                    return(
                        <tr key={i?.id} onClick={() => handleCardClick(i) } style={{cursor:"pointer"}}>
                            <th scope="row">{i?.id}</th>
                            <td>{`${i?.firstName} ${i?.lastName}`}</td>
                            <td>{i?.contactNo}</td>
                            <td>{i?.address}</td>
                        </tr>
                    )
                  })
            }
          </tbody>
      </table>
          <Modal isOpen={isModalOpen} onClose={() => setIsModalOpen(false)} data={selectedCard} />
      </div>
  )
}
