import React, { useState } from 'react'

export default function Search({ data, onSearch, onClear }) {
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearch = () => {
    // Filter the data based on the search term and pass it to the callback
    const filteredData = data.filter((item) => {
      console.log("yes")
      // Modify the condition based on your data structure
      return (
        item.firstName.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.lastName.toLowerCase().includes(searchTerm.toLowerCase())
      );
    });

    onSearch(filteredData);
  }
  const handleClear = () => {
    setSearchTerm(''); // Clear the search input
    onSearch(data); // Show all data
  };

  return (
    <div className="row mt-3">    
      <div className="col-md-8">
      </div>
      <div className="col-md-4">
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Search..."
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
          <div className="input-group-append">
            {searchTerm && (
              <button
                className="btn btn-secondary"
                type="button"
                onClick={handleClear}
              >
                Clear
              </button>
            )}
            <button
              className="btn btn-primary"
              type="button"
              onClick={handleSearch}
            >
              Search
            </button>
          </div>
        </div>
      </div>
        
</div>
  )
}
