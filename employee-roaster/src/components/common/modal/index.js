function Modal({ isOpen, onClose, data }) {
    if (!isOpen) {
        return null;
    }

    return (
        <div>
         <div
        className={`modal-overlay`}
        onClick={onClose}
        style={{
          position: 'fixed',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          background: 'rgba(0, 0, 0, 0.5)', // Semi-transparent black overlay
          zIndex: 999, // Ensure the overlay appears above other content
        }}
      ></div>
        <div
            className={`modal fade ${isOpen ? 'show' : ''}`}
            tabIndex="-1"
            role="dialog"
            style={{ display: isOpen ? 'block' : 'none' }}
        >
                <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Employee Detail:</h5>
                            <button type="button" className="btn-close" onClick={onClose}>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="row">
                                <div className="col-md-6"><img src={data?.avatar} alt={`${data?.firstName} ${data?.lastName}`}/>
                                <p>Job Title:{data?.jobTitle}</p>
                                <p>Age:{data?.age}</p>
                                <p>Joining Date:{data?.dateJoined}</p>

                                </div>
                            <div className="col-md-6"> 
                                <p>Name: {`${data?.firstName} ${data?.lastName}`}</p>
                                <p>Contact No: {data?.contactNo}</p>
                                <p>Address: {data?.address}</p>
                                <p>Bio: {data?.bio}</p></div>
                        </div>
                       

                    </div>
                </div>
            </div>
        </div>
        </div>
    );
}
export default Modal