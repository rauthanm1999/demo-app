export const fetchData = () => {
    return async (dispatch) => {
        try {
            const response = await fetch('http://localhost:3001/get-json'); // Adjust the path if needed
            const data = await response.json();
            dispatch({ type: 'FETCH_DATA_SUCCESS', payload: data });
        } catch (error) {
            dispatch({ type: 'FETCH_DATA_FAILURE', payload: error });
        }
    };
};
