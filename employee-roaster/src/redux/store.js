import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers'; // Your combined reducers
import thunk from 'redux-thunk'; // Middleware, if needed

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;