import React, { useCallback, useEffect, useState } from 'react'
import Table from "../../components/common/table"
import Pagination from "../../components/common/pagination"
import Search from "../../components/common/search"
export default function EmployeeList({ employeeData }) {
  const [currentPage, setCurrentPage] = useState(1);
  const [searchTerm, setSearchTerm] = useState("")
  const itemsPerPage = 10; // You can adjust this based on your needs.
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = employeeData?.slice(indexOfFirstItem, indexOfLastItem);

  const onPageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const [filteredData, setFilteredData] = useState(); // Initialize with all data
  const handleSearch = useCallback((searchResults) => {
    setFilteredData(searchResults);
    // You can also reset the current page to the first page when searching
    setCurrentPage(1);
  }, [filteredData])

  const clearSearch = () => {
    setSearchTerm(''); // Clear the search term in the parent component
    setFilteredData(currentItems); // Reset the filtered data to all data
  };
  return (
    <div className="container">
      <Search data={employeeData} onSearch={handleSearch} searchTerm={searchTerm}
        onClear={clearSearch} />
      <Table data={filteredData ? filteredData : currentItems} />
      <Pagination
        itemsPerPage={itemsPerPage}
        totalItems={ employeeData?.length}
        currentPage={currentPage}
        onPageChange={onPageChange}
        showText={true}
      />

    </div>
  )
}
