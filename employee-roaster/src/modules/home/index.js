import Header from '../../components/header';
import EmployeeList from '../../modules/employeeList';
import { fetchData } from '../../redux/action/action';
import { connect } from 'react-redux';
import { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";

function Home() {
    const dispatch=useDispatch()
    const { data } = useSelector((state) => state.dataReducer);

    useEffect(() => {
        dispatch(fetchData())
    }, [dispatch]);

    return (
        <>
            <Header companyInfo={data?.companyInfo} />
            <EmployeeList employeeData={data?.employees} />
        </>
    );
}
export default Home;
