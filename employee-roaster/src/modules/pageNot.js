import React from 'react'

export default function PageNot() {
  return (
      <div className='text-center container'>
          <h5 className='text-center mt-5 mb-5'>
              Page Not Found
            </h5>
          <a href="/"><button type="button" className="btn btn-primary">Back to home</button></a>  </div>
  )
}
