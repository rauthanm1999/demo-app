# http://localhost:3000=>Frontend

# http://localhost:3001=>Backend
# Employee Roaster Frontend

This is the frontend part of the Employee Roaster application.

## Prerequisites

Before running the application, make sure you have the following installed:

- [Node.js](https://nodejs.org/)
- npm (Node Package Manager)

## Getting Started

Follow these steps to set up and run the frontend application:

1. Open a terminal and navigate to the `/employee-roaster` directory.

2. Run the following command to install the project dependencies:

   ```bash
   npm install

To create a README file for your frontend code and provide instructions for running it along with the backend server, follow the steps below. You'll create a README file for your /employee-roaster frontend and provide instructions for both frontend and backend.

First, navigate to your /employee-roaster directory where your frontend code is located.

Create a new README.md file or edit an existing one.

# Employee Roaster Frontend

This is the frontend part of the Employee Roaster application.

## Prerequisites

Before running the application, make sure you have the following installed:

- [Node.js](https://nodejs.org/)
- npm (Node Package Manager)

## Getting Started

Follow these steps to set up and run the frontend application:

1. Open a terminal and navigate to the `/employee-roaster` directory.

2. Run the following command to install the project dependencies:

   ```bash
   npm install
Start the frontend application with the following command:
npm start


The frontend application should now be running. You can access it in your web browser at http://localhost:3000.

Backend Server
This frontend application is designed to work with a backend server. To set up and run the backend server, follow the instructions below.

Employee Roaster Backend
This is the backend part of the Employee Roaster application.

Prerequisites
Before running the backend server, make sure you have the following installed:

Node.js
Getting Started
Follow these steps to set up and run the backend server:

Open a terminal and navigate to the root directory of your project.

Run the following command to install the server dependencies:


cd server
npm install
Start the backend server with the following command:


node server.js
The backend server should now be running and listening on a specified port.


