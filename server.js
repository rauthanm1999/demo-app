const express = require('express');
const app = express();
const port = 3001;
const cors = require('cors');
const fs = require('fs');
const path = require('path');
app.use(cors())

app.get('/get-json', (req, res) => {
    const jsonFilePath = path.join(__dirname, 'sample-data.json'); // Adjust the path if needed
    fs.readFile(jsonFilePath, 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            res.status(500).send('Error reading JSON file');
        } else {
            res.json(JSON.parse(data));
        }
    });
});


app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
